import { Component, OnInit } from '@angular/core';
import { el } from 'date-fns/locale';
import Swal from 'sweetalert2';
import { CartService } from '../services/cart.service';
import { ListproductService } from '../services/listproduct.service';
import { Product } from '../shared/product';

@Component({
  selector: 'app-listproduct',
  templateUrl: './listproduct.component.html',
  styleUrls: ['./listproduct.component.css']
})
export class ListproductComponent implements OnInit {
  listproduct:any;

  constructor(private apilistproduct:ListproductService, private cartService:CartService) { }

  ngOnInit(): void {
    this.getalllistproduct();

  }
  getalllistproduct(){
    this.apilistproduct.getalllistproduct().subscribe((res: Product[])=>{
  console.log(res)
  this.listproduct = [];
  res.forEach((element: Product) => {
    const product = new Product();
    product.client = element.client;
    product.description = element.description;
    product.entreprise = element.entreprise;
    product.id = element.id;
    product.image = element.image;
    product.name = element.name;
    product.prix = element.prix;
    product.remise = element.remise;
    product.subcategory = element.subcategory;
    product.qte_user = 1;
    product.quantite = element.quantite;
    this.listproduct.push(product);
  });
  console.log("list product",this.listproduct)
})

  }

  removeQte(i: any) {
    const item = this.listproduct.find((o:any) => o.id === i.id);
    if(item){
      item.qte_user--;
    }
  }

  
  addQte(i: any) {
    const item = this.listproduct.find((o:any) => o.id === i.id);
    if(item){
      item.qte_user++;
    }
  }


  addToCart(item:any) {
   
    if (!this.cartService.itemInCart(item)) {

      console.log("fgggg")
      // item.qtyTotal = 1;
      this.cartService.addToCart(item); //add item in cart
   
      Swal.fire('product added')
    }
  }

}


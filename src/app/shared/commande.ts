import { Cart } from "./cart";
import { Client } from "./client";

export class Commande {
  cart?: Cart ;
  date?: string;
  payement?: any;
  id?: number;
  client? : Client;
  delivery_adresse?: string;
  delivery_date?: string;
  status?: string;
  constructor(cart?: Cart,  date?: string, payement?: any, id?: number, client? : Client, delivery_adresse?: string, status?: string){
   this.cart = cart;
   this.date = date;
   this.payement= payement;
   this.id = id ;
   this.client = client ;
   this.delivery_adresse = delivery_adresse;
   this.status = status;
  }
}
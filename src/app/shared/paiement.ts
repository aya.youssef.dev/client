import { Commande } from "./commande";

export class Paiement {
  
  commande?: Commande;
  description?: string;
  id?: number;
  date?: string;
  totale?: string;
  cash?: boolean;
}
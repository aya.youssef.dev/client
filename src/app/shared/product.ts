import { Client } from "./client";
import { Entreprise } from "./entreprise";
import { Subcategory } from "./subcategory";

export class Product {
  client?: Client;
  description: string;
  entreprise?: Entreprise;
  id: number;
  image: string;
  name?: string;
  prix: string;
  quantite: number;
  qte_user: number = 0;
  remise: number;
  subcategory? : Subcategory;
}
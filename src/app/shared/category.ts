import { Subcategory } from "./subcategory";

export class Category {
  subcategory?: Subcategory;
  description: string;
  id: number;
  name: string;
  genre: string;
 
} 
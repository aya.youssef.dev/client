import { Commande } from "./commande";
import { Product } from "./product";

export class Cart {
  id?: number;
  total?: string;
  article?: Product[];

  constructor(id?:number, total?:string){
    this.id = id; 
    this.total = total;
  }
}
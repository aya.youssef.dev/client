
export class Client {
  
   id: number;
   firstName: string;
   lastName: string;
   email: string;
   password: string;
   role: string;
   username: string;
   adresse: string;
   tel: string;
   article?: any;
   commande?: any;
   city : string;
   state: string;
   country: string ;

}
import { Entreprise } from "./entreprise";
import { Promotion } from "./promotion";
import { Subcategory } from "./subcategory";

export class Service {
  subcategory?: Subcategory;
  description: string;
  entreprise?: Entreprise;
  promotion?:  Promotion;
  id: number;
  tel: string;
  nameservice: string;
  descriptionservice: string;
  prixservice: string;
  adresse: string;

}
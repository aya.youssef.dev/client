import { Product } from "./product";
import { Service } from "./service";

export class Entreprise {
  
   id: number;
   firstName: string;
   lastName: string;
   email: string;
   password: string;
   role: string;
   username: string;
   adresse: string;
   tel: string;
   article?: Product;
   servvice?: Service;

}
import { Category } from "./category";
import { Product } from "./product";
import { Service } from "./service";

export class Subcategory {
  article?: Product;
  description: string;
  categorie?: Category;
  id: number;
  namesub: string;
  servvice? : Service;
}
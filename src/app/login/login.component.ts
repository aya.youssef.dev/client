import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  
    constructor(private apilogin:LoginService,private formBuilder: FormBuilder,private route:Router) { }
  
    ngOnInit(): void {
    
      localStorage.removeItem('userconnect');
      localStorage.removeItem('token');
      localStorage.removeItem("state");
      localStorage.removeItem("cart_items");

      this.loginForm = this.formBuilder.group({
        username: ['', Validators.required],
        password: ['', Validators.required],
        
       
    });
  }
   
       

  
  onsubmit(){
  
    this.apilogin.login(this.loginForm.value).subscribe((res:any)=>{
    console.log(res)
    if(res.user.enabled === true){
      localStorage.setItem('userconnect',JSON.stringify(res.user))
      localStorage.setItem('token',res.access_token)
      localStorage.setItem("state","0")
    Swal.fire({
      position: 'top-end',
      icon: 'success',
      title: 'Logged in successfully !',
      showConfirmButton: false,
      timer: 1500
    })
      window.location.href="http://localhost:4200"
    }
   }, err=>{
    Swal.fire({
      icon:'error',
      title:'user not found ',
      text:'Login invalid',
      footer:'password invalid'
    })
    console.log(err)
   })
  }
    
  
 
  }

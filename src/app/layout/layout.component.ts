import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../services/category.service';
import { ListproductService } from '../services/listproduct.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  category:any;
  listproduct:any
  listproductclient:any
search_name:any
  constructor(private apicategory:CategoryService,private apilistproduct:ListproductService) { }

  ngOnInit(): void {
    this.getallcategory()
    this.getalllistproduct()
    this.getalllistproductclient()
  }
  getallcategory(){
    this.apicategory.getallcategory().subscribe((res:any)=>{
  console.log(res)
  this.category=res
  console.log("list category",this.category)
    })
  }

  getalllistproductclient(){
    this.apilistproduct.getalllistproduct().subscribe((res:any)=>{
  console.log(res)
  this.listproductclient =res.filter((el:any)=>el.client!=null)
  console.log("list product client",this.listproductclient)
 
  });


  }


  getalllistproduct(){
    this.apilistproduct.getalllistproduct().subscribe((res:any)=>{
  console.log(res)
  this.listproduct =res
  console.log("list product",this.listproduct)
 
  });


  }
}

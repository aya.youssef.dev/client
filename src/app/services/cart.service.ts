import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Cart } from '../shared/cart';
import { Product } from '../shared/product';
import { environment } from 'src/environments/environment';
import { Client } from '../shared/client';
import { Commande } from '../shared/commande';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  items: Product[];
  totalPrice = 0;
  
  constructor(private http:HttpClient) {}
  addedItem:any
  
  addToCart(addedItem:any) {
    if(this.items === undefined){
      this.items = [];
    }
    this.items.push(addedItem);
    this.saveCart();
  }

  itemInCart(item:any): boolean {
    if(this.items === undefined){
      return false;
    }
    return this.items.findIndex((o:any) => o.id === item.id) > -1;
  }
  
  loadCart(): Product[] {
    return this.items = JSON.parse(localStorage.getItem("cart_items")!) ?? [];
  }
  
  getItems() {
    return this.items;
  }

  saveCart(): void {
    localStorage.setItem('cart_items', JSON.stringify(this.items));
  }

  removeItem(item:any) {
    console.log(this.items)
    const index = this.items.findIndex((o:any)=> o._id === item._id);
    if (index > -1) {
      this.items.splice(index, 1);
      this.saveCart();
    }
  }

  savePanier(panier: Cart): Observable<Cart>{
    return this.http.post<Cart>(`${environment.baseurl}/users/panier/save/`, panier);
  }
  
  saveCommande(commande: Commande, panier: Cart, client: Client){
    return this.http.post(`${environment.baseurl}/users/commande/save/${panier.id}/${client.id}`, commande);
  }


}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Product } from '../shared/product';

@Injectable({
  providedIn: 'root'
})
export class ListproductService {


  constructor(private http:HttpClient) { 

  }
  
  getalllistproduct(): Observable<Product[]>{
    return this.http.get<Product[]>(`${environment.baseurl}/users/article/all`)
  }

  getoneproduct(id:any): Observable<Product>{
    return this.http.get<Product>(`${environment.baseurl}/users/article/getone/${id}`)
  }
  

}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http:HttpClient) { }
  getallcategory(){
    return this.http.get(`${environment.baseurl}/users/categorie/all`)
  }

  deletecategory(idcategory:any){
    return this.http.delete(`${environment.baseurl}/users/categorie/delete/${idcategory}`)
  }

  createcategory(category:any){
    return this.http.post(`${environment.baseurl}/users/categorie/save`,category)
  }
}

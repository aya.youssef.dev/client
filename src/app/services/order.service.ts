import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Commande } from '../shared/commande';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) { }


  getAllOrders(userId: number): Observable<Commande[]>{
    return this.http.get<Commande[]>(`${environment.baseurl}/users/commande//allUserCommandes/${userId}`)
  }
}

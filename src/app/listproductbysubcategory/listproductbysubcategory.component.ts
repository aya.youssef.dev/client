import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ListproductService } from '../services/listproduct.service';

@Component({
  selector: 'app-listproductbysubcategory',
  templateUrl: './listproductbysubcategory.component.html',
  styleUrls: ['./listproductbysubcategory.component.css']
})
export class ListproductbysubcategoryComponent implements OnInit {
  listproduct:any
  id=this.activeroute.snapshot.params['id']
  constructor(private apilistproduct:ListproductService,private activeroute:ActivatedRoute) { }

  ngOnInit(): void {
    this.getalllistproduct()
  }


  getalllistproduct(){
    this.apilistproduct.getalllistproduct().subscribe((res:any)=>{
  console.log(res)
  this.listproduct =res
  .filter((el:any)=>el.subcategory.id==this.id)
  console.log("list product",this.listproduct)
 
  });


  }


}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListproductbysubcategoryComponent } from './listproductbysubcategory.component';

describe('ListproductbysubcategoryComponent', () => {
  let component: ListproductbysubcategoryComponent;
  let fixture: ComponentFixture<ListproductbysubcategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListproductbysubcategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListproductbysubcategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

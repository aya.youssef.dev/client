import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CartComponent } from './cart/cart.component';
import { CategoryComponent } from './category/category.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { HomeComponent } from './home/home.component';
import { LayoutComponent } from './layout/layout.component';
import { ListproductComponent } from './listproduct/listproduct.component';
import { ListproductbysubcategoryComponent } from './listproductbysubcategory/listproductbysubcategory.component';
import { LoginComponent } from './login/login.component';
import { OrderComponent } from './order/order.component';
import { ProductdetailComponent } from './productdetail/productdetail.component';
import { RegisterComponent } from './register/register.component';
import { SubcategoryComponent } from './subcategory/subcategory.component';

const routes: Routes = [
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {  path:'',component:HomeComponent,children:[
      {path:'',component:LayoutComponent},
      {path:'listproduct',component:ListproductComponent},
      {path:'productdetail/:id',component:ProductdetailComponent},
      {path:'cart',component:CartComponent},
      {path:'category',component:CategoryComponent},
      {path:'subcategory/:id',component:SubcategoryComponent},
      {path:'listproductbysubcategory/:id',component:ListproductbysubcategoryComponent},
      {path:'checkout',component:CheckoutComponent},
      {path:'orders/:status',component:OrderComponent},


    ]}
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

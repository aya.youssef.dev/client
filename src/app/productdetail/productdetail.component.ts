import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { CartService } from '../services/cart.service';
import { ListproductService } from '../services/listproduct.service';
import { Product } from '../shared/product';

@Component({
  selector: 'app-productdetail',
  templateUrl: './productdetail.component.html',
  styleUrls: ['./productdetail.component.css']
})
export class ProductdetailComponent implements OnInit {
  id=this.activeroute.snapshot.params['id']
  product: Product;
  baseUrl= "http://localhost:8083/users/files/"
  imageUrl= "";
  qte_user:number=1;
  items = [] as any;
  constructor(private activeroute:ActivatedRoute,private apiproduct:ListproductService,private cartService:CartService) { }

  async ngOnInit(): Promise<void> {
   
    console.log(this.id)
   await this.getoneproduct();

  }
  async getoneproduct(){
    this.apiproduct.getoneproduct(this.id).subscribe((res: Product) => {
      if (res) {
        console.log(res);
        this.product = res;
        this.imageUrl = this.baseUrl + this.product.id + ".jpg";
        console.log(" product", this.product);
      }
    });
  } 
  plus(){
    if(this.qte_user<this.product.quantite){
    this.qte_user++;
    }
  } 

 munis(){
    this.qte_user--;
  } 

  addToCart(item:any) {
   
    if (!this.cartService.itemInCart(item)) {

      console.log("fgggg")
      // item.qtyTotal = 1;
      item.qte_user=this.qte_user;
      this.cartService.addToCart(item); //add item in cart
      this.items =this.cartService.getItems();
      console.log(this.items)
      Swal.fire('product added')
    }
  }


}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { RegisterService } from '../services/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  fileToUpload:Array<File>=[]

  userconnect=JSON.parse(localStorage.getItem('userconnect')!)


  constructor(private apiregister:RegisterService,private formBuilder: FormBuilder,private route:Router) { }

  ngOnInit(): void {

    
      
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      username: ['', Validators.required],
      adresse: ['', Validators.required],
      tel: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      city: ['', Validators.required],
      country: ['', Validators.required],
      state: ['', Validators.required],

      
     
   });
}
 
     
handleFileInput(files: any){
  this.fileToUpload=<Array<File>>files.target.files;
  console.log(this.fileToUpload);
}

getoneclient(){
  
}

onsubmit(){
  let formdata=new FormData();
  formdata.append("lastName",this.registerForm.value.lastName);
  formdata.append("fistName",this.registerForm.value.fistName);
  formdata.append("adresse",this.registerForm.value.adresse);
  formdata.append("tel",this.registerForm.value.tel);
  formdata.append("city",this.registerForm.value.city);
  formdata.append("state",this.registerForm.value.state);
  formdata.append("country",this.registerForm.value.country);
  formdata.append("email",this.registerForm.value.email);
  formdata.append("username",this.registerForm.value.username);
  formdata.append("password",this.registerForm.value.password);
  formdata.append("file",this.fileToUpload[0]);

 this.apiregister.register(formdata).subscribe((res:any)=>{
 console.log(res)
 Swal.fire(' client registred  ')
  this.route.navigateByUrl('/login')

 })
 }
  


}

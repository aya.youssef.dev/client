import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OrderService } from '../services/order.service';
import { Commande } from '../shared/commande';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
status: string;
commandes: Commande[] = [];

  constructor(private activeroute:ActivatedRoute, private orderService: OrderService) {
    this.status = this.activeroute.snapshot.params['status']
   }

  ngOnInit(): void {
    const user =  JSON.parse(localStorage.getItem("userconnect")!) ?? undefined;
    if(user){
      this.orderService.getAllOrders(user.id).subscribe((commandes: Commande[]) => {
        console.log('all commandes ==================> ', commandes)
        if(this.status === 'all') {
            this.commandes = commandes;
        } else {
          console.log('this.status ==================> ', this.status)

          commandes.forEach((commande: Commande) => {
            console.log('commande.status ==================> ', commande.status)
            if(commande.status === this.status){
              this.commandes.push(commande);
            }
          });
        }       
        console.log('commandes à afficher ==================> ', this.commandes)
      });
    }

  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SubcategoryService } from '../services/subcategory.service';

@Component({
  selector: 'app-subcategory',
  templateUrl: './subcategory.component.html',
  styleUrls: ['./subcategory.component.css']
})
export class SubcategoryComponent implements OnInit {
  listsubcategory:any
  id=this.activeroute.snapshot.params['id']
  constructor(private activeroute:ActivatedRoute,private apisubcategory:SubcategoryService) { }

  ngOnInit(): void {
    this.getalllistsubcategory()
  }
  getalllistsubcategory(){
    this.apisubcategory.getallsubcategory().subscribe((res:any)=>{
  console.log(res)
  this.listsubcategory=res.filter((el:any)=>el.categorie.id==this.id)
  console.log("list subcategory",this.listsubcategory)
    })
  }
  

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { CategoryService } from '../services/category.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  category:any;
  categoryForm: FormGroup;
  constructor(private apicategory:CategoryService,private formBuilder: FormBuilder) { }

  ngOnInit(): void {
      this.getallcategory()
      this.categoryForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
     
  });

  }
getallcategory(){
  this.apicategory.getallcategory().subscribe((res:any)=>{
console.log(res)
this.category=res
console.log("list category",this.category)
  })
}
deletecategory(id:any){
  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.isConfirmed) {

      this.apicategory.deletecategory(id).subscribe((res:any)=>{
        console.log(res)
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
        this.getallcategory()
      })
      Swal.fire(
        'Deleted!',
        'Your file has been deleted.',
        'success'
      )
      this.getallcategory()
    }
  })
}
    createcategory(){
      this.apicategory.createcategory(this.categoryForm.value).subscribe((res:any)=>{
      console.log(res)
      this.getallcategory()

     })
    }
    
}

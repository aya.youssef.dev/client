import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CalendarView, CalendarEvent, DateAdapter } from 'angular-calendar';
import { startOfDay } from 'date-fns';
import { subscribeOn } from 'rxjs';
import Swal from 'sweetalert2';
import { CartService } from '../services/cart.service';
import { Cart } from '../shared/cart';
import { Commande } from '../shared/commande';
import { Paiement } from '../shared/paiement';
import { Product } from '../shared/product';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  items : Product[] = [];
  totalPrice = 0;
  adress = "";
  wadress = "";
  commande: Commande;
  viewDate: Date;
  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;
  user:  any;
  state=JSON.parse(localStorage.getItem("state")!)
  constructor(private apicart:CartService,private route:Router) { }

  ngOnInit(): void {    
   this.apicart.loadCart();
    this.initItems();
    console.log(this.items);
    this.getAdresses();
    this.viewDate =  new Date();
    this.initCommande();
  }

  initItems() {
    this.items = this.apicart.getItems();
  }
  
  dayClicked({ date }: { date: Date;  }): void {
    console.log(date);
    console.log(' dayClicked this.commande ==> ', this.commande)
    this.commande.date = new Date().toString();
    this.commande.delivery_date = date.toString();
    
  }

 
  addDeliveryAdressToCommande(adresse: string) {    
    this.commande.delivery_adresse = adresse;
  }

  initCommande() {
    this.commande = new Commande();
    this.commande.cart = undefined ;
    this.commande.date = "";
    this.commande.payement = undefined ;
    this.commande.client = undefined ;
    this.commande.delivery_adresse = undefined;
    const paiement = new Paiement();
    this.commande.payement = paiement;
    this.commande.status = 'En attente de validation';
  }

  getAdresses() {
    this.user =  JSON.parse(localStorage.getItem("userconnect")!) ?? undefined;
   if(this.user){
    this.adress = this.user.adresse;
    this.wadress = this.user.workAdresse;
   }
  }

  getTotalSansRemise() {
    let total = 0;
    this.items.forEach((element:any) => {
      total += Number(element.prix) * Number(element.qte_user);
      //  this.totalDeliveryAmt= element.price * element.Qte;
    });
    return total;
  }

  gettotal() {
    let total = 0;
    this.items.forEach((element:any) => {
      total += Number(element.remise) * Number(element.qte_user);
      //  this.totalDeliveryAmt= element.price * element.Qte;
    });
    return total;
  }

  getCharges() {

    return this.getTotalSansRemise() - this.gettotal();
  }

  removeQte(i: any) {
    const item = this.items.find((o:Product) => o.id === i.id);
    const itemIndex = this.items.findIndex((o:Product) => o.id === i.id);
    if(item && item.qte_user){
      item.qte_user--;
       if (item.qte_user === 0){
        this.items.splice(itemIndex, 1);
       }
    }
  }

  
  addQte(i: any) {
    const item = this.items.find((o:any) => o.id === i.id);
    if(item  && item.qte_user){
      item.qte_user++;
    }
  }

  saveCommande(){
    console.log("state",this.state)
  if(this.state==null){
   this.route.navigateByUrl('/login')
   return
  }
 
    if ( this.state==0 && this.commande.cart && this.commande.client)
       this.apicart.saveCommande( this.commande,  this.commande.cart,  this.commande.client).subscribe();
       Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Merci pouyr votre achat ! vous pouvez suivre le statut de votre commande dans MyOrder !',
        showConfirmButton: false,
        timer: 1500
      });
      localStorage.removeItem('cart_items');
        window.location.href="http://localhost:4200/listproduct";
    
  }
   
  updatePaiement(){
    this.commande.payement.date =  new Date();
    this.commande.payement.total = this.gettotal() + 10;
    this.commande.payement.cash = true;
  }

  savePanier(){
    const cart = new Cart();
    cart.article = this.items;
    cart.total = (this.gettotal() + 10).toString();
    this.commande.cart = cart;
    this.commande.client = this.user;
      this.apicart.savePanier(this.commande.cart).subscribe((cart: Cart) => {
        if(this.commande.cart)
          this.commande.cart.id = cart.id;

    });
    }


}


